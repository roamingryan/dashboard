// ***************************************************
// chart_live.js
//
// author: Ryan Kingsbury
//
// Creates a chart in the specified HTML <div> and then updates
// the chart in realtime.
//
// ***************************************************

// This function updates the series on the chart
function updateSeries(s)
{
    var chart = $(chart_id).highcharts();

    // Create request data structure
    var data = new Array()
    for (var i=0; i<chart.series.length; i++) {
	var series = chart.series[i]
	data.push({pkt_name: pkt_name, field_name: series.name, num_points: numPoints})
    }

    // Perform the AJAX call
    $.ajax({
    	type: "POST",
    	url: "/plot_data",
    	data: JSON.stringify(data),
        contentType: "application/json",
        processData: false,
    	success: function(json_str) {
	    var chart = $(chart_id).highcharts();

	    // Parse the JSON response
	    var resp = JSON.parse(json_str)
 	    for (i=0; i<resp.length; i++) {
		var data = resp[i]
 		var series = chart.get(data.name)
     		series.setData(data.data)
	    }

	    // Schedule updateSeries() to run again after the desired delay
	    setTimeout(updateSeries,interval)
    	}
    });    
}

$(document).ready(function() {
    // Create the chart
    $(chart_id).highcharts({
    	chart: {events: {load: function () {
    	    // After the chart has loaded, call updateSeries()
	    updateSeries()
    	}}},
    	title: title,
    	series: series,
	// Disable mouse-over data point information
	tooltip: {
	    enabled: false,
	}
    });
});

