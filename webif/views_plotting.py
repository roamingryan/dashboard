from flask import render_template, request, session, flash, url_for, redirect, jsonify

from webif import app
import json

# Displays list of available plots    
@app.route('/plotting/')
def available_plots_list():
    pkttypes = app.tlm_db.keys()
    return render_template('available_plots.html', pkttypes=pkttypes, title="Available plots")

# Configure or render the plot
# HTTP GET: presents user with form so that they can select desired fields
# HTTP POST: renders HTML page with plot
@app.route('/plotting/<name>', methods=['GET','POST'])
def form_select_plot_series(name):
    """ Presents a form so that the user can select which data series
    to plot.

    TODO: Add x-axis selection
    TODO: Option to include table of latest packet numbers
    """
    
    schema = app.tlm_db[name][-1].copy()

    if request.method == "GET":
        return render_template('plot_form.html',schema=schema, title="%s plot setup"%name)

    # If this is a post, then deal with the incoming form data
    if request.method == 'POST':
        plot_config = {"pkt_name": name,
                       "title": {"text":name},
                       "chartOpts": {"type":"line"},
                       "numPoints": int(request.form['num_points']),
                       "interval": int(request.form['interval'])}

        series = []
        # Check to see which of the fields (or field array elements)
        # have been selected for plotting
        for f in schema['fields']:
            # Skip padding fields
            if f['name']=='__padding':
                continue
                
            # If this isn't an array field, then we just see if the
            # field is selected
            if 'array' not in f:
                if f['name'] in request.form:
                    series_descriptor = {"name": f['name'],
                                         "id": f['name']}
                    series.append(series_descriptor)
            else:
                # Values of arrays are POSTed in the format of "NAME,element#"
                # This formatting is defined in the HTML template (cmd_form.html)
                v_list = []
                for elem in range(f['array']):
                    field_name = "%s,%i"%(f['name'],elem)
                    if field_name in request.form:
                        series_descriptor = {"name":"%s[%i]"%(f['name'],elem),
                                             "id":  "%s[%i]"%(f['name'],elem)}
                        series.append(series_descriptor)

        plot_config['series'] = series
        return render_template("chart.html",plot_config=plot_config,title="%s plot"%name)

# Returns JSON-formatted chart data
# This function is called by chart_live.js
@app.route('/plot_data', methods=['POST'])
def get_series_data():
    """ Returns data for the specified series.  Series is specified as
    JSON object.

    Example input: To get second value of temp array from RTD_RAW packet...
                   {"pkt_name": "RTD_RAW",
                    "field_name": "temp[2]"
                   }

    Example output: see extract_series() documentation below
    """
    
    if request.method == 'POST':

        # Convert the posted JSON back into a python object
        reqs = json.loads(request.data)
        resp_list = []
        for r in reqs:
            # Extract the request series data from the TLM database
            series_data = extract_series(r['pkt_name'],
                                         r['field_name'],
                                         r['num_points'])
            resp_list.append(series_data)

        return json.dumps(resp_list)
        
    return "error"

def extract_series(pkt_name,field_name,num_points = None):
    """ Extracts the specified telemetry field from the recent
    telemetry database.  Result is returned as a list of values or as
    a list of (time, value) pairs.

    Example (no time field): 
       return { name: "vec[0]",
                data: [ 1,2,3,4 ]
              }

    TODO: Implement time_field
    Example (with time field): 
       return { name: "vec[0]",
                data: [ [timestr,1],
                        [timestr,2],
                        [timestr,3],
                        [timestr,4],
                      ]    
              }

    """
    # Grab a copy of the packet schema
    schema = app.tlm_db[pkt_name][-1].copy()

    series = {}
    series['data'] = []
    series['name'] = field_name

    array=False
    if "[" in field_name:
        # There has got to be a nicer way to do this...
        array_idx = int(field_name.split('[')[1][:-1])
        field_name= field_name.split('[')[0]
        array=True
    
    # Extract the selected value from the telemetry database
    if num_points:
        pkt_list = app.tlm_db[pkt_name][-num_points:]
    else:
        pkt_list = app.tlm_db[pkt_name]
    
    for pkt in pkt_list:
        # If the parameter has been formatted (via a formatting function
        # specified in the JSON schema), then plot that.
        if field_name in pkt['values_formatted']:
            if not array:
                d = pkt['values_formatted'][field_name]
            else:
                d = pkt['values_formatted'][field_name][array_idx]
        # Otherwise just plot the raw value
        else:
            if not array:
                d = pkt['values'][field_name]
            else:
                d = pkt['values'][field_name][array_idx]

        series['data'].append(d)

    return series

