from flask import render_template, request, session, flash, url_for, redirect

from webif import app
import json

@app.route("/")
def main_page():
    return redirect(url_for('telemetry_list'))

@app.route("/stats/deframer")
def deframer_stats():
    return json.dumps(app.transport.protocol.pkt_deframer.stats,
                      sort_keys=True, indent=4, separators=(',', ': '))

@app.route("/stats/framer")
def framer_stats():
    return json.dumps(app.transport.protocol.pkt_framer.stats)

@app.route("/link_stats")
def link_stats():
    fs = app.transport.protocol.pkt_framer.stats
    dfs = app.transport.protocol.pkt_deframer.stats
    return render_template("link_stats.html", fs=fs, dfs=dfs,title="Link Stats")

@app.route("/navbar_live")
def navbar_live():
    fs = app.transport.protocol.pkt_framer.stats
    dfs = app.transport.protocol.pkt_deframer.stats
    return render_template("navbar_live.html", fs=fs, dfs=dfs)
    
@app.route('/tlmdb')
def dump_tlm_db():
    return json.dumps(app.tlm_db)

@app.route('/tlm')
def telemetry_list():

    # Only pass the single most recent telemetry packets of each type
    recentdb = {}
    for pkttype in app.tlm_db:
        recentdb[pkttype] = app.tlm_db[pkttype][-1].copy()
        
    return render_template("tlm_list.html",db=recentdb,title="TLM Summary")

@app.route('/tlm/<name>/')
def telemetry_packet_live(name = None):
    if name not in app.tlm_db:
        return "Unknown packet type: %s"%name
    return render_template("tlm_live.html",name=name, pkt=app.tlm_db[name][-1],title="%s table"%name)
    
@app.route('/tlm/<name>/data')
def telemetry_packet_live_data(name = None):
    if name not in app.tlm_db:
        return "Unknown packet type: %s"%name
    return render_template("tlm_live_data.html",name=name, pkt=app.tlm_db[name][-1])

@app.route('/tlm/<name>/recent/<num>')
def telemetry_packet_recent(name = None, num=10):
    numint = int(num)
    if name not in app.tlm_db:
        return "Unknown packet type: %s"%name
    return render_template("tlm_recent.html",name=name, pktlist=app.tlm_db[name][-numint:],title="Recent %s packets"%name)

    
@app.route('/cmd')
def command_list():

    builder = app.transport.protocol.pkt_builder
    
    return render_template("cmd_list.html",cmds=builder.schema.keys(),title="CMD list" )
    
    
@app.route('/cmd/<name>', methods=['GET','POST'])
def commmand_form(name = None):
    # If this is an HTTP set, then return the packet schema so that the form can be rendered
    if request.method == 'GET':
        schema = app.transport.protocol.pkt_builder.schema[name]
        return render_template("cmd_form.html",schema=schema, title=name)

    # If this is a post, then deal with the incoming form data
    if request.method == 'POST':
        schema = app.transport.protocol.pkt_builder.schema[name]
        schema['values']={}

        # Parse all the fields
        for f in schema['fields']:
            # Skip padding fields
            if f['name']=='__padding':
                continue
        
            # If this isn't an array field, then we just get the value
            if 'array' not in f:
                schema['values'][f['name']] = request.form[f['name']]
                continue
            else:
                # Values of arrays are POSTed in the format of "NAME,element#"
                # This formatting is defined in the HTML template (cmd_form.html)
                v_list = []
                for elem in range(f['array']):
                    field_name = "%s,%i"%(f['name'],elem)
                    v_list.append( request.form[field_name] )
                schema['values'][f['name']] = v_list
      
        # Validate command fields
        pktb = app.transport.protocol.pkt_builder
        cmd_schema = pktb.validate_packet(schema)

        # Transmit the packet
        app.transport.protocol.transmitPacket(cmd_schema)

        msg = "Command sent: " + pktb.pretty_print_packet(schema)
        flash(msg)
        return redirect(url_for('command_list'))

