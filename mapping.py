####################################################################
# Data Type Mapping
#
# author: Ryan Kingsbury
#
# This module contains the appropriate data type mappings for the
# target embedded system.  The default values below are appropriate
# for the Microchip PIC XC16 compiler.
#
####################################################################

def get_mappings():

    # Mapping of C data types to python struct format strings.
    # This is where we tell Python about the PIC's definitions of data
    # types.  These values are used by the python struct module.
    DATA_TYPE_FMT = { "int8_t"     : "b",
                      "uint8_t"    : "B",
                      "int16_t"    : "h",
                      "uint16_t"   : "H",
                      "int32_t"    : "i",
                      "uint32_t"   : "I",
                      "int64_t"    : "q",
                      "uint64_t"   : "Q",
                      "float"      : "f",
                      "double"     : "f",  # PIC treats doubles as single-precision
                   }

    return DATA_TYPE_FMT

    