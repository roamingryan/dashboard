####################################################################
# Dashboard
#
# author: Ryan Kingsbury
#
# This application provides a web-based command and telemetry
# interface to an embedded system.
#  * raw logging of binary data from hardware
#  * Web-based interface for display of TLM and generation of CMDs
#
# For command line help:
#     python dashboard.py --help
#
####################################################################

# External libraires
import Queue
import os
import json
from datetime import datetime
from twisted.internet.protocol import Protocol, ClientFactory, ReconnectingClientFactory
from twisted.internet.serialport import SerialPort
        
# Custom libraries/modules
import pktlog
import deframer
import pktparser
import framer
import pktbuilder

# Global data structures
HISTORY_LENGTH = 1000 # number of TLM/CMD packets to hold in memory

# Output log file naming
LOG_PATH = "logs"
LOG_BASE_NAME = datetime.utcnow().strftime("%Y%m%d_%H%M%S") # time string

class HardwareProtocol(Protocol):
    """ Implements our custom framing protocol through use of the
    deframer and framer modules. """

    # Recent telemetry database
    db = {}

    # Data reception objects
    raw_pkt_queue = Queue.Queue()
    pkt_deframer = deframer.BinaryPacketReceiver(raw_pkt_queue)
    
    # making framer a class var so it can be overridden in child classes 
    pkt_parser = pktparser.Parser()
    
    # Data transmission objects
    pkt_builder = pktbuilder.Builder()
    # pkt_framer is made after the connection is established (see makeConnection() below)
    
    tlm_logfile = open(os.path.join(LOG_PATH,LOG_BASE_NAME+".tlm.log"),'w')
    cmd_logfile = open(os.path.join(LOG_PATH,LOG_BASE_NAME+".cmd.log"),'w')

    def transmitPacket(self, pkt):

        # Build the packet
        pktbuilt = self.pkt_builder.build_packet(pkt)

        # Send the packet
        pktsent = self.pkt_framer.xmit_packet(pktbuilt)
        txtime = datetime.utcnow()

        # Log the raw packet to a file
        # Note: file I/O could technically block so we really
        # shouldn't be doing this here as it violates the main
        # tenet of async. programming.  That said, file I/O
        # should be fast so we'll ignore this matter for now.
        # TODO: use twisted's non-blocking logging infrastructure
        entry = pktlog.make_entry_from_pkt(txtime,pktsent)
        self.cmd_logfile.write( entry )
        
    def makeConnection(self, transport):
        
        # Establish the connection (either serial or TCP)
        Protocol.makeConnection(self,transport)
        
        # Once the connection has been made, attach it to the flask app
        flaskapp = transport.protocol.factory.flaskapp
        setattr(flaskapp,"tlm_db",transport.protocol.db)
        setattr(flaskapp,"transport",transport)

        linkmode = transport.protocol.factory.linkmode
        
        # Create a framer object
        if linkmode is "serial":
            self.pkt_framer = framer.SerialFramer(transport)
        elif linkmode is "cadet":
            self.pkt_framer = framer.CadetFramer(transport)
        else:
            raise "link mode is invalid"

            
    def update_tlm_db(self, pkt):
        """ Updates the telemetry database structure."""
        
        # Make sure database has the necessary key (packet name), if
        # not create one.
        if pkt['name'] not in self.db:
            self.db[pkt['name']] = []

        # Append the new packet to the database, keeping only the most
        # recent N packets
        self.db[pkt['name']] = self.db[pkt['name']][-(HISTORY_LENGTH-1):]+[pkt]

        print pkt['name'], json.dumps(pkt['values'], encoding="latin-1")
        
        return
        
    def dataReceived(self, data):
        """ This function is called whenever the transport layer
        receives bytes."""
        # Process received bytes
        for b in data:
            self.pkt_deframer.recv_byte(ord(b))

        # If there are packet(s) waiting in the queue, deal with them
        while True:
            try:
                pktraw = self.raw_pkt_queue.get(block=False)

                # Log the raw packet to a file
                # Note: file I/O could technically block so we really
                # shouldn't be doing this here as it violates the main
                # tenet of async. programming.  That said, file I/O
                # should be fast so we'll ignore this matter for now.
                # TODO: use twisted's non-blocking logging infrastructure
                rxtime = datetime.utcnow()
                entry = pktlog.make_entry_from_string(rxtime,pktraw)
                self.tlm_logfile.write( entry )

                pkt = self.pkt_parser.parse_packet(pktraw)

                # If return type is None, then something went wrong
                if pkt is None:
                    print "Failed to parse packet: [",
                    for b in pktraw:
                        print " 0x%X"%ord(b),
                    print "]"
                    continue

                # Add timestamp metadata to packet
                pkt['utc']=rxtime.isoformat()

                # Update the telemetry database
                self.update_tlm_db(pkt)

            except Queue.Empty:
                break

    def sendPacket(self):
        print "sendPacket() stub"
        return
        
    def connectionLost(self, reason):
        # Close the logfile
        print "Closing logfiles:"
        print " Telemetry: ",os.path.join(LOG_PATH,LOG_BASE_NAME+".tlm.log")
        print " Commands:  ",os.path.join(LOG_PATH,LOG_BASE_NAME+".cmd.log")
        self.tlm_logfile.close()
        self.cmd_logfile.close()
        
class HardwareProtocolFactory(ClientFactory):
    protocol = HardwareProtocol
    
    def __init__(self,flaskapp,linkmode=None):
        """The flask app needs to be provided to the factory so that
        it can be attached to newly-created protocols.
        """
        self.flaskapp = flaskapp

        # Validate, then store link mode
        if linkmode not in ['serial','cadet']:
            raise "Invalid link mode"
            
        self.linkmode = linkmode

        
def setup_flask_web_interface():
    """ Instantiates the Flask web framework."""
        
    # Import the web interface app
    from webif import app

    # run in under twisted through wsgi
    from twisted.web.wsgi import WSGIResource
    from twisted.web.server import Site
    resource = WSGIResource(reactor, reactor.getThreadPool(), app)
    site = Site(resource)
    reactor.listenTCP(8080,site)

    return app

if __name__ == '__main__':
    from twisted.internet import reactor
    import argparse

    # Define parser for command line arguments
    ap = argparse.ArgumentParser(description='Dashboard')
    ap.add_argument("port", help='serial port (e.g. /dev/ttyUSB0 or COM1)')
    ap.add_argument('--rate',
                    type=int,
                    dest='baudrate',
                    default=115200,
                    help='specify baud rate (default: %(default)s)')

    ap.add_argument('--test',
                    action='store_true',
                    dest='test',
                    help='connect to test TCP/IP server instead of serial port')

    # Parse the command line arguments
    args = ap.parse_args()

    # Setup the web interface (provided by flask)
    flaskapp = setup_flask_web_interface()

    # Special test mode for testing w/o hardware
    if args.test == True:
        # Establish connection to local hardware_sim.py server
        factory = HardwareProtocolFactory(flaskapp,linkmode="serial")
        conn = reactor.connectTCP("localhost",int(args.port),factory)

    # Serial/USB link mode
    else:
        # Establish connection to satellite (either TCP or serial)
        factory = HardwareProtocolFactory(flaskapp,linkmode="serial")
        
        # Create a detached protocol
        hwProtocol = factory.buildProtocol(None) 
        # Next, bind the protocol to a the serial port
        port = SerialPort(hwProtocol, args.port, reactor, baudrate=args.baudrate)

    # Start the twisted reactor (drives asynchronous processes)
    reactor.run()

