#!/bin/bash
# Run this to fix problems with the Pumpkin CubeSat development board
# It reload the Linux FTDI kernel module with new parameters so that
# it knows to expect the non-standard vendor/product ID used on the
# Pumpkin board.  Also brute forces the permissions on said device(s)

sudo modprobe -r ftdi_sio
sudo modprobe ftdi_sio vendor=0x0403 product=0xf020
sleep 1
sudo chmod 777 /dev/ttyUSB*

